const express = require('express');

const app = express();

const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.get('/home', (req, res)=>{
	res.send('This is home')
});

let users = [];

app.get('/users', (req, res)=>{
	res.send(users)
})

app.post('/users', (req, res)=>{
	res.send(`Fist Name: ${req.body.firstName},
		Last Name: ${req.body.lastName},
		email: ${req.body.email}`);
});

app.listen(port, ()=> console.log(`Server running at port ${port}`))